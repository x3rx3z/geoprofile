#!/usr/bin/python3
from math import pow
'''
Geoprofiliing based on Rossmo's formula

Rossmo's formula is a geographic profiling formula to predict where a serial criminal lives [1].
The formula has been applied to fields other than forensics. [1]
In this case, repurposed to predict a user's location, given a network association footprint

Sam Clarke - March - 2018


1. https://en.wikipedia.org/wiki/Rossmo%27s_formula
'''

buffer_dist = 0

param_f = 1
param_g = 2

crimes=[(70,70), (80,20), (20,80), (10, 10)]

# Probability for a given point
p = 0

# x, y for the point being calculated
# i, j for the nth crime site
def phi( x, y, i, j ):
    if abs(x-i) + abs(y-j) > buffer_dist:
        return 1
    else:
        return 0

# total range of search x,y coordinates
for x in range(100):
    for y in range(100):
        # Sum Rossmo's over each crime
        for crime in crimes:
            i = crime[0]
            j = crime[1]

            # Get characteristic phi
            characteristic = phi(x, y, i, j)

            # Calculate the Mannhattan distance between point & crime
            manhattan_dist = abs(x-i) + abs(y-j)

            # Calculate for term of Rossmo's
            try:
                p += characteristic / pow(manhattan_dist, param_f)
            except ZeroDivisionError:
                pass

            # Calculate second term of Rossmo's
            n = (1-characteristic)*pow(buffer_dist, (param_g-param_f))
            d = (2*buffer_dist)-characteristic
            d = pow(d, param_g)

        print("X: "+str(x)+", Y: "+str(y)+", P: "+str(p))
        p = 0
    print("")








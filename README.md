Geoprofiliing based on Rossmo's formula

Rossmo's formula is a geographic profiling formula to predict where a serial criminal lives [1].
The formula has been applied to fields other than forensics. [1]
In this case, repurposed to predict a user's location, given a network association footprint

Sam Clarke - COMP6841 Project - March 2018


1. https://en.wikipedia.org/wiki/Rossmo%27s_formula

![alt text](img/img.png)